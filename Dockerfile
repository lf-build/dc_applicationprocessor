FROM registry.lendfoundry.com/base:beta8

ADD ./src/DivergentCapital.ApplicationProcessor.Abstractions /app/DivergentCapital.ApplicationProcessor.Abstractions
WORKDIR /app/DivergentCapital.ApplicationProcessor.Abstractions
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN dnu build

ADD ./src/DivergentCapital.ApplicationProcessor.Persistence /app/DivergentCapital.ApplicationProcessor.Persistence
WORKDIR /app/DivergentCapital.ApplicationProcessor.Persistence
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN dnu build

ADD ./src/DivergentCapital.ApplicationProcessor /app/DivergentCapital.ApplicationProcessor
WORKDIR /app/DivergentCapital.ApplicationProcessor
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN dnu build

ADD ./src/DivergentCapital.ApplicationProcessor.Api /app/DivergentCapital.ApplicationProcessor.Api
WORKDIR /app/DivergentCapital.ApplicationProcessor.Api
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN dnu build

EXPOSE 5000
ENTRYPOINT dnx kestrel
﻿using CreditExchange.Syndication.Plaid.Response;
using LendFoundry.Business.Application;
using LendFoundry.Foundation.Services;
using RestSharp;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using DivergentCapital.ApplicationProcessor.Request;
using LendFoundry.Syndication.Paynet.Response;
using LendFoundry.Syndication.Experian.Response;
using LendFoundry.Business.OfferEngine;
using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport;

namespace DivergentCapital.ApplicationProcessor.Client
{
    public class ApplicationProcessorClientService : IApplicationProcessorClientService
    {
        public ApplicationProcessorClientService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<IApplicationResponse> SubmitBusinessApplication(ISubmitBusinessApplicationRequest submitBusinessApplicationRequest)
        {
            var request = new RestRequest("submit/business", Method.POST);
            request.AddJsonBody(submitBusinessApplicationRequest);
            return await Client.ExecuteAsync<ApplicationResponse>(request);
        }

        public async Task<ITransactionsAndAccountsResponse> PlaidCalculateCashFlow(string entityId, IPlaidBankAccountRequest plaidBankAccountRequest)
        {
            var request = new RestRequest("plaid/cashflow/{entityid}", Method.POST);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(plaidBankAccountRequest);
            return await Client.ExecuteAsync<TransactionsAndAccountsResponse>(request);
        }

        public async Task<ITransactionsAndAccountsResponse> PlaidReCalculateCashFlow(string entityId)
        {
            var request = new RestRequest("plaid/recalculatecashflow/{entityid}", Method.POST);
            request.AddUrlSegment("entityid", entityId);

            return await Client.ExecuteAsync<TransactionsAndAccountsResponse>(request);
        }      

        public async Task<IApplicationResponse> AddLead(ISubmitBusinessApplicationRequest submitApplicationRequest)
        {
            var request = new RestRequest("lead", Method.POST);
            request.AddJsonBody(submitApplicationRequest);
            return await Client.ExecuteAsync<ApplicationResponse>(request);
        }

        public async Task<bool> SubmitDocument(string entityId, string category, byte[] fileBytes, string fileName, List<string> tags)
        {
            var objRequest = new RestRequest("{entityid}/{category}/{*tags}", Method.POST);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddUrlSegment("category", category);
            AppendTags(objRequest, tags);
            objRequest.AddFile("file", fileBytes, fileName, "application/octet-stream");
            return await Client.ExecuteAsync<bool>(objRequest);
        }

        public async Task<bool> SignConsent(string entityId, string category, object body)
        {
            var objRequest = new RestRequest("{entityid}/{category}/consent/sign", Method.POST);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddUrlSegment("category", category);
            objRequest.AddParameter("body", body);
            return await Client.ExecuteAsync<bool>(objRequest);
        }

        private static void AppendTags(IRestRequest request, IReadOnlyList<string> tags)
        {
            if (tags == null || !tags.Any())
                return;

            var url = request.Resource;
            for (var index = 0; index < tags.Count; index++)
            {
                var tag = tags[index];
                url = url + $"/{{tag{index}}}";
                request.AddUrlSegment($"tag{index}", tag);
            }
            request.Resource = url;
        }

        public async Task<IGetSearchBusinessResponse> BusinessSearch(string entityId, IBusinessSearchRequest request)
        {
            var objRequest = new RestRequest("{entityid}/experian/business/search", Method.POST);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<GetSearchBusinessResponse>(objRequest);
        }

        public async Task<IGetBusinessReportResponse> GetBusinessReport(string entityId, IBusinessReportRequest request)
        {
            var objRequest = new RestRequest("{entityid}/experian/business/report", Method.POST);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddParameter("request", request);
            return await Client.ExecuteAsync<GetBusinessReportResponse>(objRequest);
        }

        public async Task<IGetPersonalReportResponse> GetPersonalReport(string entityId, IPersonalReportRequest request)
        {
            var objRequest = new RestRequest("{entityid}/report/personal", Method.POST);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<GetPersonalReportResponse>(objRequest);
        }

        public async Task<string> ChooseOffer(string entityId, string offerId)
        {
            var objRequest = new RestRequest("{entityid}/{offerid}/offers/final", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddUrlSegment("offerId", offerId);
            return await Client.ExecuteAsync<string>(objRequest);
        }

        public async Task<string> PerformBusinessVerification1(string entityId)
        {
            var objRequest = new RestRequest("{entityid}/businessverification1", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<string>(objRequest);
        }

        public async Task<string> PerformBusinessVerification2(string entityId)
        {
            var objRequest = new RestRequest("{entityid}/businessverification2", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<string>(objRequest);
        }

        public async Task<string> ResendBankLinking(string entityId)
        {
            var objRequest = new RestRequest("{entityid}/resend/banklink", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<string>(objRequest);
        }

        public async Task<IApplicationOffer> ComputeOffer(string entityId)
        {
            var objRequest = new RestRequest("{entityid}/compute/offer", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<ApplicationOffer>(objRequest);
        }

        public async Task<string> CalculateManualCashflow(string entityId)
        {
            var objRequest = new RestRequest("plaid/cashflow/manual/{entityid}", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<string>(objRequest);
        }

        public async Task<string> EditOffer(string entityId)
        {
            var objRequest = new RestRequest("{entityid}/edit/offer", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<string>(objRequest);
        }

        public async Task<IList<IBusinessLoanOffer>> PresentOffer(string entityId)
        {
            var objRequest = new RestRequest("{entityid}/present/offer", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<IList<IBusinessLoanOffer>>(objRequest);
        }

        public async Task<string> GenerateAndSendAgreement(string entityId)
        {
            var objRequest = new RestRequest("{entityid}/generate/agreement", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<string>(objRequest);
        }

        public async Task<string> StipulateDocs(string entityId)
        {
            var objRequest = new RestRequest("{entityid}/stipulate/docs", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<string>(objRequest);
        }

        public async Task<ISearchCompanyResponse> SearchCompany(string entityId, ISearchCompanyRequest request)
        {
            var objRequest = new RestRequest("{entityid}/paynet/search", Method.POST);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<SearchCompanyResponse>(objRequest);
        }

        public async Task<bool> GetPaynetReport(string entityId, string paynetId)
        {
            var objRequest = new RestRequest("{entityid}/{paynetId}/paynet/report", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddUrlSegment("paynetId", paynetId);
            return await Client.ExecuteAsync<bool>(objRequest);
        }

        public async Task<string> PerformCreditCheckVerification(string entityId)
        {
            var objRequest = new RestRequest("{entityid}/creditcheckverification", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<string>(objRequest);
        }

        public async Task<string> PerformBusinessTaxIdVerification(string entityId)
        {
            var objRequest = new RestRequest("{entityid}/businesstaxidverification", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<string>(objRequest);
        }

        public async Task<string> PerformBusinessReviewCreditRisk(string entityId)
        {
            var objRequest = new RestRequest("{entityid}/businessreviewcreditrisk", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<string>(objRequest);
        }

        public async Task<IApplicationDetails> GetApplicationDetails(string entityId)
        {
            var request = new RestRequest("{entityid}/details", Method.GET);
            request.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<ApplicationDetails>(request);
        }

        public async Task<IApplicationDetails> EditApplication(string entityId, ISubmitBusinessApplicationRequest request)
        {
            var objRequest = new RestRequest("{entityid}/edit", Method.PUT);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<ApplicationDetails>(objRequest);
        }

        public async Task<IApplicationOffer> AddDeal(string entityId, IDealOfferRequest request)
        {
            var objRequest = new RestRequest("{entityid}/add/deal", Method.POST);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<ApplicationOffer>(objRequest);
        }

        public async Task<IBankruptcySearchResponse> BankruptcySearch(string entityId, Request.ISearchBankruptcyRequest request)
        {
            var objRequest = new RestRequest("{entityid}/bankruptcy/search", Method.POST);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<BankruptcySearchResponse>(objRequest);
        }

        public async Task<IBankruptcyReportResponse> BankruptcyReport(string entityId, Request.IBankruptcyReportRequest request)
        {
            var objRequest = new RestRequest("{entityid}/bankruptcy/report", Method.POST);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<BankruptcyReportResponse>(objRequest);
        }

        public async Task<ICriminalRecordSearchResponse> CriminalRecordSearch(string entityId, ICriminalRecordSearchRequest request)
        {
            var objRequest = new RestRequest("{entityid}/criminalrecord/search", Method.POST);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<CriminalRecordSearchResponse>(objRequest);
        }

        public async Task<ICriminalReportResponse> CriminalRecordReport(string entityId, ICriminalRecordReportRequest request)
        {
            var objRequest = new RestRequest("{entityid}/criminalrecord/report", Method.POST);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<CriminalReportResponse>(objRequest);
        }

        public Task<string> DocuSignEventListner(string entityId, IDocusignEventRequest docusignEventRequest)
        {
            throw new NotImplementedException();
        }

        public async Task<string> AddFundingRequest(string entityid)
        {
            var objRequest = new RestRequest("{entityid}/add/fundingrequest", Method.POST);
            objRequest.AddUrlSegment("entityid", entityid);
            return await Client.ExecuteAsync<string>(objRequest);
        }
    }
}
﻿using LendFoundry.Security.Tokens;

namespace DivergentCapital.ApplicationProcessor.Client
{
    public interface IApplicationProcessorClientServiceFactory
    {
        IApplicationProcessorClientService Create(ITokenReader reader);
    }
}

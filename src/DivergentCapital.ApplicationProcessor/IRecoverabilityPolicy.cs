﻿using System;

namespace DivergentCapital.ApplicationProcessor
{
    public interface IRecoverabilityPolicy
    {
        bool CanBeRecoveredFrom(Exception exception);
    }
}

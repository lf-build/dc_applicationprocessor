﻿using CreditExchange.Foundation.Validator;
using CreditExchange.StatusManagement;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DivergentCapital.ApplicationProcessor
{
    public class StatusManagementService : IStatusManagementService
    {
        public StatusManagementService(IEntityStatusService entityStatusService, ILookupService lookup, ApplicationProcessorConfiguration applicationProcessorConfiguration)
        {
            if (entityStatusService == null)
                throw new ArgumentNullException(nameof(entityStatusService));

            EntityStatusService = entityStatusService;
            EntityValidator = new EntityValidator(lookup);
            ApplicationProcessorConfiguration = applicationProcessorConfiguration;
        }
      

        private IEntityStatusService EntityStatusService { get; }
        private ApplicationProcessorConfiguration ApplicationProcessorConfiguration { get; }
        private IEntityValidator EntityValidator { get; }      

        public async Task ChangeStatus(string entityId, string newStatus, List<string> reasons)
        {
            EnsureInputIsValid(entityId);
            await Task.Run(() => EntityStatusService.ChangeStatus("application", entityId, newStatus, reasons));


        }

        public async Task RejectApplication(string entityId, List<string> reasons)
        {
            EnsureInputIsValid(entityId);
            await Task.Run(() => EntityStatusService.ChangeStatus("application", entityId, ApplicationProcessorConfiguration.Statuses["Rejected"], reasons));
        }

        public async Task ApplicationStatusMove(string applicationNumber)
        {
            EnsureInputIsValid(applicationNumber);
            var currentApplicationStatus = EntityStatusService.GetStatusByEntity("application", applicationNumber);
            if (currentApplicationStatus == null)
                throw new NotFoundException($"Current Status of Application Number {applicationNumber} can not be found");

            if (currentApplicationStatus.Code == ApplicationProcessorConfiguration.Statuses["FinalofferAccepted"])
                await Task.Run(() => EntityStatusService.ChangeStatus("application", applicationNumber, ApplicationProcessorConfiguration.Statuses["CEApproved"]));
            else if (currentApplicationStatus.Code == ApplicationProcessorConfiguration.Statuses["RBLFraud"])
                await Task.Run(() => EntityStatusService.ChangeStatus("application", applicationNumber, ApplicationProcessorConfiguration.Statuses["BankCreditReview"]));
            else if (currentApplicationStatus.Code == ApplicationProcessorConfiguration.Statuses["BankCreditReview"])
                await Task.Run(() => EntityStatusService.ChangeStatus("application", applicationNumber, ApplicationProcessorConfiguration.Statuses["Approved"]));
        }   

        public async Task<IEnumerable<IActivity>> GetActivities(string applicationNumber)
        {

            EnsureInputIsValid(applicationNumber);
            var currentApplicationStatus = EntityStatusService.GetStatusByEntity("application", applicationNumber);
            if (currentApplicationStatus == null)
                throw new NotFoundException($"Current Status of Application Number {applicationNumber} can not be found");

            return await Task.Run(() => EntityStatusService.GetActivities("application", currentApplicationStatus.Code));

        }
        private void EnsureInputIsValid(string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

        }

        public static JToken FindToken<T>(string key, string value)
        {

            var jObject = JObject.Parse(value);
            var jToken = jObject.SelectToken(key);
            if (jToken != null)
                return jToken;

            return null;
        }
    }
}

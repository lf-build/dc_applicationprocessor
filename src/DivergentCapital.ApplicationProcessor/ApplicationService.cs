﻿using CreditExchange.Application.Document;
using CreditExchange.Consent;
using CreditExchange.Email;
using CreditExchange.StatusManagement;
using CreditExchange.Syndication.BBBSearch;
using CreditExchange.Syndication.Datamerch;
using CreditExchange.Syndication.Plaid;
using CreditExchange.Syndication.Plaid.Response;
using CreditExchange.Syndication.Whitepages;
using CreditExchange.Syndication.Yelp;
using CreditExchange.VerificationEngine;
using DivergentCapital.ApplicationProcessor.Events;
using DivergentCapital.ApplicationProcessor.Request;
using LendFoundry.Business.Applicant;
using LendFoundry.Business.Application;
using LendFoundry.Business.OfferEngine;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Lookup;
using LendFoundry.ProductRule;
using LendFoundry.Syndication.Experian;
using LendFoundry.Syndication.Experian.Request;
using LendFoundry.Syndication.Experian.Response;
using LendFoundry.Syndication.LexisNexis;
using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport;
using LendFoundry.Syndication.LFDocuSign;
using LendFoundry.Syndication.Paynet;
using LendFoundry.Syndication.Paynet.Response;
using Microsoft.CSharp.RuntimeBinder;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DivergentCapital.ApplicationProcessor
{
    public class ApplicationService : IApplicationProcessorClientService
    {
        #region Constructor

        public ApplicationService(ApplicationProcessorConfiguration applicationProcessorConfiguration, IEntityStatusService entityStatusService,
           ILookupService lookupService,
            IApplicationService businessApplicationService,
            IDataMerchService dataMerchService,
            IYelpService yelpService,
            IWhitepagesService whitepagesService,
            IEmailService emailService,
            IBBBSearchService bbbSearchService,
            IPlaidService plaidService,
            LendFoundry.DataAttributes.IDataAttributesEngine dataAttributeService,        
            IEventHubClient eventHub,
            IApplicationDocumentService applicationDocumentService,
            IConsentService consentService,
            LendFoundry.Syndication.Experian.IBusinessReportService businessReportService,
            IPersonalReportService personalReportService,
            IVerificationEngineService verificationEngineService,
            IDecisionEngineService decisionEngineService,
            IDocuSignService docuSignService,
            IPaynetService paynetReportService,
            IApplicantService businessApplicantService,
            IOfferEngineService offerEngineService,
            ILexisNexisService lexisNexisService,
            IProductRuleService productRuleService
            )
        {
            if (businessApplicationService == null)
                throw new ArgumentNullException(nameof(businessApplicationService));

            if (businessApplicantService == null)
                throw new ArgumentNullException(nameof(businessApplicantService));

            if (entityStatusService == null)
                throw new ArgumentNullException(nameof(entityStatusService));

            if (dataMerchService == null)
                throw new ArgumentNullException(nameof(dataMerchService));

            if (yelpService == null)
                throw new ArgumentNullException(nameof(yelpService));

            if (bbbSearchService == null)
                throw new ArgumentNullException(nameof(bbbSearchService));

            if (whitepagesService == null)
                throw new ArgumentNullException(nameof(whitepagesService));

            if (emailService == null)
                throw new ArgumentNullException(nameof(emailService));

            if (plaidService == null)
                throw new ArgumentNullException(nameof(plaidService));

            if (dataAttributeService == null)
                throw new ArgumentNullException(nameof(dataAttributeService));      

            if (eventHub == null)
                throw new ArgumentNullException(nameof(eventHub));

            if (applicationDocumentService == null)
                throw new ArgumentNullException(nameof(applicationDocumentService));

            if (consentService == null)
                throw new ArgumentNullException(nameof(consentService));

            if (businessReportService == null)
                throw new ArgumentNullException(nameof(businessReportService));

            if (personalReportService == null)
                throw new ArgumentNullException(nameof(personalReportService));

            if (verificationEngineService == null)
                throw new ArgumentNullException(nameof(verificationEngineService));

            if (decisionEngineService == null)
                throw new ArgumentNullException(nameof(decisionEngineService));
            if (paynetReportService == null)
                throw new ArgumentNullException(nameof(paynetReportService));

            if (docuSignService == null)
                throw new ArgumentNullException(nameof(docuSignService));

            if (offerEngineService == null)
                throw new ArgumentNullException(nameof(offerEngineService));

            if (productRuleService == null)
                throw new ArgumentNullException(nameof(productRuleService));

            LookupService = lookupService;
            BusinessApplicationService = businessApplicationService;
            BusinessApplicantService = businessApplicantService;
            EntityStatusService = entityStatusService;
            ApplicationProcessorConfiguration = applicationProcessorConfiguration;
            DataMerchService = dataMerchService;
            YelpService = yelpService;
            BBBSearchService = bbbSearchService;
            WhitepagesService = whitepagesService;
            EmailService = emailService;
            PlaidService = plaidService;
            DataAttributeService = dataAttributeService;          
            EventHub = eventHub;
            ApplicationDocumentService = applicationDocumentService;
            ConsentService = consentService;
            BusinessReportService = businessReportService;
            PersonalReportService = personalReportService;
            VerificationEngineService = verificationEngineService;
            DecisionEngineService = decisionEngineService;
            DocuSignService = docuSignService;
            PaynetReportService = paynetReportService;
            OfferEngineService = offerEngineService;
            LexisNexisService = lexisNexisService;
            ProductRuleService = productRuleService;
        }

        #endregion Constructor

        #region Private Variables

        private ILookupService LookupService { get; }
        private ApplicationProcessorConfiguration ApplicationProcessorConfiguration { get; }

        private IEntityStatusService EntityStatusService { get; }

        private LendFoundry.DataAttributes.IDataAttributesEngine DataAttributeService { get; }

        private IApplicationService BusinessApplicationService { get; }

        private IApplicantService BusinessApplicantService { get; }

        private IDataMerchService DataMerchService { get; }
        private IYelpService YelpService { get; }
        IBBBSearchService BBBSearchService { get; }

        private IWhitepagesService WhitepagesService { get; }

        private ITenantTime TenantTime { get; }

        private IEmailService EmailService { get; }

        private IPlaidService PlaidService { get; }

        private IEventHubClient EventHub { get; set; }

        private IApplicationDocumentService ApplicationDocumentService { get; set; }

        private IConsentService ConsentService { get; set; }

        private LendFoundry.Syndication.Experian.IBusinessReportService BusinessReportService { get; set; }

        private IPersonalReportService PersonalReportService { get; set; }
        private IPaynetService PaynetReportService { get; set; }

        private IVerificationEngineService VerificationEngineService { get; set; }

        private IDecisionEngineService DecisionEngineService { get; set; }

        private IDocuSignService DocuSignService { get; set; }

        private IOfferEngineService OfferEngineService { get; }

        private ILexisNexisService LexisNexisService { get; set; }

        private IProductRuleService ProductRuleService { get; set; }

        private string EntityType { get; set; } = "application";

        #endregion Private Variables
        private string ExtractEmail(ISubmitBusinessApplicationRequest submitBusinessApplicationRequest)
        {
            string Email = string.Empty;
            if (submitBusinessApplicationRequest.Owners != null && submitBusinessApplicationRequest.Owners.Count > 0)
            {
                if (submitBusinessApplicationRequest.Owners[0].EmailAddresses != null && submitBusinessApplicationRequest.Owners[0].EmailAddresses.Count > 0)
                {
                    Email = submitBusinessApplicationRequest.Owners[0].EmailAddresses[0].Email;
                }
            }

            return Email;
        }

        #region Business Application

        public async Task<IApplicationResponse> SubmitBusinessApplication(ISubmitBusinessApplicationRequest submitBusinessApplicationRequest)
        {
            string Email = ExtractEmail(submitBusinessApplicationRequest);

            EnsureBusinessInputIsValid(submitBusinessApplicationRequest,Email);

            var applicationRequest = GetBusinessApplicationRequest(submitBusinessApplicationRequest,Email);

            // Application Submitted
            var application = await BusinessApplicationService.Add(applicationRequest);

            EntityStatusService.ChangeStatus(EntityType, application.ApplicationNumber, ApplicationProcessorConfiguration.Statuses["ApplicationSubmitted"]);

            #region Experian - Personal

            //First Owner details taking for personal report
            var objPersonalReportResponse = await CallExperianPersonalSyndication(application.ApplicationNumber, submitBusinessApplicationRequest.Owners[0].FirstName, submitBusinessApplicationRequest.Owners[0].LastName, submitBusinessApplicationRequest.Owners[0].SSN,
                submitBusinessApplicationRequest.Owners[0].Addresses[0].AddressLine1, submitBusinessApplicationRequest.Owners[0].Addresses[0].City,
                submitBusinessApplicationRequest.Owners[0].Addresses[0].State, submitBusinessApplicationRequest.Owners[0].Addresses[0].ZipCode);

            var referencenumber = Guid.NewGuid().ToString("N");
            await EventHub.Publish(new CreditCheckVerification
            {
                EntityId = application.ApplicationNumber,
                EntityType = EntityType,
                Response = null,
                Request = submitBusinessApplicationRequest.Owners[0].FirstName,
                ReferenceNumber = referencenumber
            });

            var eligibilityResult = VerifyRule(EntityType, application.ApplicationNumber, RuleType.PScoreComputation, GroupName.PScoreCheck, RuleName.CheckPScore, null, ApplicationProcessorConfiguration.DataAttributes.PScoreAttributes);

            #endregion Experian - Personal

            #region Eligibility Check

            if (EligibilityCheck(EntityType, application.ApplicationNumber) == true)
            {
                //Dictionary<string, object> objMultiResponse = new Dictionary<string, object>();
                var referencenumber1 = Guid.NewGuid().ToString("N");                        

                await EventHub.Publish(new BusinessVerification1
                {
                    EntityId = application.ApplicationNumber,
                    EntityType = EntityType,
                    Response = null,
                    Request = "SubmitBusinessApplication",
                    ReferenceNumber = referencenumber1
                });

                #region Email - Bank Verification

                SendBankVerificationEmail(application.ApplicationNumber, Email, applicationRequest.ContactFirstName, applicationRequest.ContactLastName);

                #endregion Email - Bank Verification                

                #region PScore Rule

                var objPScore = await DataAttributeService.GetAttribute(EntityType, application.ApplicationNumber, new List<string> { ApplicationProcessorConfiguration.DataAttributes.PScoreAttributes });
                //An application will be declined is his / her p_grade in (15,16,17,18,19 20) 
                if (objPScore == null || objPScore.Count == 0)
                    throw new ArgumentException("Data attribute is not available");
                var p_score = objPScore.FirstOrDefault().Value;
                int p_grade = Convert.ToInt32(GetPgradeValue(p_score));
                if (p_grade >= 15 && p_grade <= 20)
                {
                    // change status to application declined
                    List<string> reasons = new List<string>();
                    reasons.Add("r9");
                    EntityStatusService.ChangeStatus(EntityType, application.ApplicationNumber, ApplicationProcessorConfiguration.Statuses["Declined"], reasons);
                    return application;
                }
                #endregion PScore Rule

                
                // change status to application draft
                EntityStatusService.ChangeStatus(EntityType, application.ApplicationNumber, ApplicationProcessorConfiguration.Statuses["Verification"]);

            }
            else
            {
                // change status to application declined

                List<string> reasons = new List<string>();
                reasons.Add("r9");
                EntityStatusService.ChangeStatus(EntityType, application.ApplicationNumber, ApplicationProcessorConfiguration.Statuses["Declined"], reasons);
            }

            #endregion Eligibility Check

            return application;
        }

        public async Task<IApplicationResponse> AddLead(ISubmitBusinessApplicationRequest submitApplicationRequest)
        {
            string Email = ExtractEmail(submitApplicationRequest);
            EnsureBusinessInputIsValid(submitApplicationRequest,Email);
            var applicationRequest = GetBusinessApplicationRequest(submitApplicationRequest,Email);
            var application = await BusinessApplicationService.Add(applicationRequest);
            EntityStatusService.ChangeStatus(EntityType, application.ApplicationNumber, ApplicationProcessorConfiguration.Statuses["LeadCreated"]);
            return application;
        }


        private static string GetPgradeValue(dynamic data)
        {
            Func<dynamic> resultProperty = () => data.p_grade;
            return HasProperty(resultProperty) ? GetValue(resultProperty) : null;
        }

        #endregion Business Application


        #region Plaid Methods

        public async Task<ITransactionsAndAccountsResponse> PlaidCalculateCashFlow(string entityId, IPlaidBankAccountRequest plaidBankAccountRequest)
        {
            CreditExchange.Syndication.Plaid.Request.ExchangeTokenRequest objExchangeTokenRequest = new CreditExchange.Syndication.Plaid.Request.ExchangeTokenRequest();
            objExchangeTokenRequest.PublicToken = plaidBankAccountRequest.PublicToken;

            var objPlaidExchangeTokenResponse = await PlaidService.GetExchangeToken(EntityType, entityId, objExchangeTokenRequest);

            if (objPlaidExchangeTokenResponse != null)
            {
                CreditExchange.Syndication.Plaid.Request.AccountsRequest objAccountRequest = new CreditExchange.Syndication.Plaid.Request.AccountsRequest();
                objAccountRequest.AccessToken = objPlaidExchangeTokenResponse.AccessToken;
                objAccountRequest.BankSupportedProductType = plaidBankAccountRequest.BankSupportedProductType;

                var objPlaidResponse = await PlaidService.CalculateCashFlow(EntityType, entityId, objAccountRequest);
                var referencenumber = Guid.NewGuid().ToString("N");
                await EventHub.Publish(new BankLinked
                {
                    EntityId = entityId,
                    EntityType = EntityType,
                    Response = objPlaidResponse,
                    Request = plaidBankAccountRequest,
                    ReferenceNumber = referencenumber
                });

                return objPlaidResponse;
            }

            return null;
        }

        public async Task<ITransactionsAndAccountsResponse> PlaidReCalculateCashFlow(string entityId)
        {
            var objAccessTokens = await DataAttributeService.GetAttribute(EntityType, entityId, new List<string> { "AccessToken", "BankSupportedProductType" });
            if (objAccessTokens != null)
            {
                CreditExchange.Syndication.Plaid.Request.AccountsRequest objAccountRequest = new CreditExchange.Syndication.Plaid.Request.AccountsRequest();
                objAccountRequest.AccessToken = objAccessTokens["accessToken"].ToString();
                objAccountRequest.BankSupportedProductType = objAccessTokens["bankSupportedProductType"].ToString();

                var objPlaidResponse = await PlaidService.CalculateCashFlow(EntityType, entityId, objAccountRequest);
                var referencenumber = Guid.NewGuid().ToString("N");
                await EventHub.Publish(new PlaidBankVerification
                {
                    EntityId = entityId,
                    EntityType = EntityType,
                    Response = null,
                    Request = "PlaidReCalculateCashFlow",
                    ReferenceNumber = referencenumber
                });
                return objPlaidResponse;
            }
            return null;
        }

        #endregion Plaid Methods        

        #region Submit Document

        public async Task<bool> SubmitDocument(string entityId, string category, byte[] fileBytes, string fileName, List<string> tags)
        {
            bool result = false;
            var objApplicationDocument = await ApplicationDocumentService.Add(entityId, category, fileBytes, fileName, tags);
            if (objApplicationDocument != null && !string.IsNullOrEmpty(objApplicationDocument.DocumentId))
            {
                result = true;
            }

            return result;
        }

        #endregion Submit Document

        #region Sign Consent

        public async Task<bool> SignConsent(string entityId, string category, object body)
        {
            bool result = false;
            var objConsent = await ConsentService.Sign(EntityType, entityId, category, body);

            if (objConsent != null && !string.IsNullOrEmpty(objConsent.DocumentId))
                result = true;
            return result;
        }

        #endregion Sign Consent

        #region Experian

        #region Business Report

        public async Task<IGetSearchBusinessResponse> BusinessSearch(string entityId, IBusinessSearchRequest request)
        {
            IGetBusinessSearchRequest objRequest = new GetBusinessSearchRequest();

            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            if (application == null)
                throw new InvalidOperationException("Application not found");

            objRequest.BusinessName = request.LegalBusinessName;
            objRequest.AlternateName = request.DBA;
            objRequest.Street = request.AddressLine1;
            objRequest.City = request.City;
            objRequest.State = request.State;
            objRequest.Zip = request.ZipCode;

            var objReport = await BusinessReportService.GetBusinessSearch(EntityType, entityId, objRequest);
            return objReport;
        }

        public async Task<IGetBusinessReportResponse> GetBusinessReport(string entityId, IBusinessReportRequest request)
        {
            IGetBusinessReportRequest objReportRequest = new GetBusinessReportRequest();

            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            if (application == null)
                throw new InvalidOperationException("Application not found");

            objReportRequest.TransactionNumber = request.TransactionNumber;
            objReportRequest.BisListNumber = request.BisListNumber;// "70044177300";
            objReportRequest.BusinessName = request.LegalBusinessName;
            objReportRequest.AlternateName = request.DBA;

            objReportRequest.Street = request.AddressLine1;
            objReportRequest.City = request.City;
            objReportRequest.State = request.State;
            objReportRequest.Zip = request.ZipCode;

            var objReportResponse = await BusinessReportService.GetBusinessReport(EntityType, entityId, objReportRequest);

            return objReportResponse;
        }

        #endregion Business Report

        #region Personal Report

        public async Task<LendFoundry.Syndication.Experian.Response.IGetPersonalReportResponse> GetPersonalReport(string entityId, IPersonalReportRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            LendFoundry.Syndication.Experian.Request.IGetPersonalReportRequest objRequest = new LendFoundry.Syndication.Experian.Request.GetPersonalReportRequest();
            objRequest.Firstname = request.Firstname;
            objRequest.Lastname = request.Lastname;
            objRequest.City = request.City;
            objRequest.Street = request.Street;
            objRequest.State = request.State;
            objRequest.Zip = request.Zip;
            objRequest.Ssn = request.Ssn;
            try
            {
                var objReport = await PersonalReportService.GetPersonalReportResponse(EntityType, entityId, objRequest);
                return objReport;
            }
            catch (System.Exception ex)
            {
                string e = ex.Message;
            }

            return null;
        }

        #endregion Personal Report

        #endregion Experian

        #region Choose Offer

        public async Task<string> ChooseOffer(string entityId, string offerId)
        {
            string result = string.Empty;

            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            if (application == null)
                throw new InvalidOperationException("Application not found");

            //var objOffersResponse = await BusinessApplicationService.GetApplicationOffers(entityId, "initialoffer");
            //if (objOffersResponse != null)
            //{
            //    var objOffers = objOffersResponse.offers;
            //    objOffers = objOffers.Where(item => item.OfferId == offerId).ToList();
            //    await BusinessApplicationService.SetOffers(entityId, "finaloffer", objOffers);

            //    ILoanAgreement body = GetDummyLoanAgreement(application, objOffers);

            //    var objConsent = await ConsentService.Sign(EntityType, entityId, "loanagreement", body);

            //    if (objConsent != null && !string.IsNullOrEmpty(objConsent.DocumentId))
            //        result = "Offer selected";
            //}

            return result;
        }

        #endregion Choose Offer

        #region Perform Business Verification1

        public async Task<string> PerformBusinessVerification1(string entityId)
        {
            string result = string.Empty;

            var referencenumber = Guid.NewGuid().ToString("N");

            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            if (application == null)
                throw new InvalidOperationException("Application not found");

            await EventHub.Publish(new BusinessVerification1
            {
                EntityId = entityId,
                EntityType = EntityType,
                Response = null,
                Request = "PerformBusinessVerification1",
                ReferenceNumber = referencenumber
            });
            result = "Business Verification1 Completed"; //TODO
            return result;
        }

        #endregion Perform Business Verification1

        #region Perform Business Verification2

        public async Task<string> PerformBusinessVerification2(string entityId)
        {
            string result = string.Empty;
            var referencenumber = Guid.NewGuid().ToString("N");

            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            if (application == null)
                throw new InvalidOperationException("Application not found");

            await EventHub.Publish(new BusinessVerification2
            {
                EntityId = entityId,
                EntityType = EntityType,
                Response = null,
                Request = "BusinessVerification2",
                ReferenceNumber = referencenumber
            });
            result = "Business Verification2 Completed"; //TODO
            return result;
        }

        #endregion Perform Business Verification2

        #region Resend Bank Linking

        public async Task<string> ResendBankLinking(string entityId)
        {
            string result = string.Empty;
            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            if (application == null)
                throw new InvalidOperationException("Application not found");

            SendBankVerificationEmail(entityId, application.PrimaryEmail.Email, application.ContactFirstName, application.ContactLastName);

            return result;
        }

        #endregion Resend Bank Linking

        #region Compute Offer

        public async Task<IApplicationOffer> ComputeOffer(string entityId)
        {
            var objInitialOffers = await OfferEngineService.ComputeOffer(EntityType, entityId);
            EntityStatusService.ChangeStatus(EntityType, entityId, ApplicationProcessorConfiguration.Statuses["OfferComputed"]);
            return objInitialOffers;
        }

        #endregion Compute Offer

        #region Calculate Manual Cashflow

        public Task<string> CalculateManualCashflow(string entityId)
        {
            return null;
        }

        #endregion Calculate Manual Cashflow

        #region Edit Offer

        public Task<string> EditOffer(string entityId)
        {
            return null;
        }

        #endregion Edit Offer

        #region Present Offer

        public async Task<IList<IBusinessLoanOffer>> PresentOffer(string entityId)
        {
            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            if (application == null)
                throw new InvalidOperationException("Application not found");

            var applicationOffers = await OfferEngineService.GetApplicationOffers(EntityType, entityId);

            EntityStatusService.ChangeStatus(EntityType, entityId, ApplicationProcessorConfiguration.Statuses["OfferGiven"]);

            return applicationOffers.Offers;
        }

        #endregion Present Offer

        #region Generate And Send Agreement

        public async Task<string> GenerateAndSendAgreement(string entityId)
        {
            var result = string.Empty;

            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            if (application == null)
                throw new InvalidOperationException("Application not found");
            var applicant = await BusinessApplicantService.Get(application.ApplicantId);
            if (applicant == null)
                throw new InvalidOperationException("Applicant not found");

            var applicationOffers = await OfferEngineService.GetApplicationOffers(EntityType, entityId);
            if (applicationOffers?.DealOffer != null)
            {
                ILoanAgreement body = GetLoanAgreementObject(application, applicant, applicationOffers.DealOffer);

                var objConsent = await ConsentService.Sign(EntityType, entityId, "loanagreement", body);

                if (objConsent != null && !string.IsNullOrEmpty(objConsent.DocumentId))
                {
                    LendFoundry.Syndication.LFDocuSign.Request.IEmbedSigningRequest objRequest = new LendFoundry.Syndication.LFDocuSign.Request.EmbedSigningRequest();
                    objRequest.DocumentId = objConsent.DocumentId;
                    objRequest.Receipients = new List<LendFoundry.Syndication.LFDocuSign.Models.IReceipient>
                        {
                            new LendFoundry.Syndication.LFDocuSign.Models.Receipient
                            {
                                Email = application.PrimaryEmail.Email,
                                Name = application.ContactFirstName + " " + application.ContactLastName
                            }
                        };
                    objRequest.ClientId = "test";

                    objRequest.EmailBody = $"Hi {body?.ContactPerson} \n\n Please review and sign your merchant agreement.";
                    var objDocuSignResponse = await DocuSignService.GenerateEmbeddedView(EntityType, entityId, objRequest);

                    if (objDocuSignResponse != null && !string.IsNullOrEmpty(objDocuSignResponse.EnvelopeSummary.EnvelopeId))
                    {
                        result = "Loan Aggrement Sent"; //TODO
                    }
                }
            }
            else result = "No deal created";
            return result;
        }

        #endregion Generate And Send Agreement

        #region Stipulate Docs

        public Task<string> StipulateDocs(string entityId)
        {
            return null;
        }

        #endregion Stipulate Docs

        #region Private Methods

        #region Call Yelp Syndication

        private async Task<CreditExchange.Syndication.Yelp.Response.ISearchResponse> CallYelpSyndication(string entityId, string phone)
        {
            var objYelpResponse = await YelpService.GetPhoneDetails(EntityType, entityId, phone);
            return objYelpResponse;
        }

        #endregion Call Yelp Syndication

        #region Call BBB Search Syndication

        private async Task<CreditExchange.Syndication.BBBSearch.Response.IBBBSearchResponse> CallBBBSearchSyndication(string entityId, string LegalBusinessName)
        {
            CreditExchange.Syndication.BBBSearch.Request.BBBSearchRequest objBBBSearchRequest = new CreditExchange.Syndication.BBBSearch.Request.BBBSearchRequest();
            objBBBSearchRequest.PageSize = 10;
            objBBBSearchRequest.PageNumber = 2;
            objBBBSearchRequest.OrganizationName = LegalBusinessName;
            objBBBSearchRequest.IsBBBAccredited = false;
            objBBBSearchRequest.IsReportable = false;
            var objBBBSearchResponse = await BBBSearchService.SearchOrganization(EntityType, entityId, objBBBSearchRequest);
            return objBBBSearchResponse;
        }

        #endregion Call BBB Search Syndication

        #region Call White Page Syndication

        private async Task<CreditExchange.Syndication.Whitepages.Response.ILeadVerifyResponse> CallWhitePageSyndication(string entityId, string firstName, string lastName, string email, string phone, string addressLine1, string city, string state, string zipcode, string legalBusinessName)
        {
            CreditExchange.Syndication.Whitepages.Request.LeadVerifyRequest objLeadVerifyRequest = new CreditExchange.Syndication.Whitepages.Request.LeadVerifyRequest();
            objLeadVerifyRequest.City = city;
            objLeadVerifyRequest.Email = email;
            objLeadVerifyRequest.FirstName = firstName;
            objLeadVerifyRequest.LastName = lastName;
            objLeadVerifyRequest.Phone = phone;
            objLeadVerifyRequest.PostalCode = zipcode;
            objLeadVerifyRequest.StateCode = state;
            objLeadVerifyRequest.StreetLine1 = addressLine1;
            objLeadVerifyRequest.CountryCode = "01";
            objLeadVerifyRequest.IpAddress = "108.194.128.165";
            objLeadVerifyRequest.Name = legalBusinessName;
            objLeadVerifyRequest.StreetLine2 = "Suite 100";
            var objWhitepagesResponse = await WhitepagesService.VerifyLead(EntityType, entityId, objLeadVerifyRequest);

            return objWhitepagesResponse;
        }

        #endregion Call White Page Syndication

        #region Call DataMerch Syndication

        private async Task<CreditExchange.Syndication.Datamerch.Response.ISearchMerchantResponse> CallDataMerchSyndication(string entityId, string businessTaxID)
        {
            var objDataMerchResponse = await DataMerchService.SearchMerchant(EntityType, entityId, businessTaxID);
            return objDataMerchResponse;
        }

        #endregion Call DataMerch Syndication

        #region Send Bank Verification Email

        private async void SendBankVerificationEmail(string entityId, string email, string firstName, string lastName, string bankType = "Plaid")
        {
            #region Email - Bank Verification

            Dictionary<string, string> objEmailData = new Dictionary<string, string>();
            objEmailData.Add("Email", email);
            objEmailData.Add("Name", firstName + " " + lastName);
            objEmailData.Add("loanNumber", entityId);
            objEmailData.Add("dateCreated", DateTime.Now.ToString());
            objEmailData.Add("ipAddress", "123.456.78.908");
            objEmailData.Add("Logo", ApplicationProcessorConfiguration.ImageConfiguration.Logo);
            objEmailData.Add("ContactAddress", ApplicationProcessorConfiguration.ImageConfiguration.ContactAddress);
            string token = entityId;//System.Net.WebUtility.UrlEncode(Security.EncryptValue(entityId));
            string queryString = string.Format("?token={0}", token);
            if (bankType == "Plaid")
            {
                objEmailData.Add("url", ApplicationProcessorConfiguration.PlaidUIEndpoint);
            }
            else if (bankType == "Yodlee")
            {
                objEmailData.Add("url", ApplicationProcessorConfiguration.YodleeUIEndpoint);
            }
            objEmailData.Add("queryString", queryString);

            await EmailService.Send(EntityType, entityId, "BankLinkVerificationTemplate", "1.0", objEmailData, null);

            #endregion Email - Bank Verification
        }

        #endregion Send Bank Verification Email

        #region Credit Check Verification

        private async Task<IGetPersonalReportResponse> CallExperianPersonalSyndication(string entityId, string firstName, string lastName, string ssn, string address, string city, string state, string zipCode)
        {
            LendFoundry.Syndication.Experian.Request.IGetPersonalReportRequest objPersonalReportRequest = new LendFoundry.Syndication.Experian.Request.GetPersonalReportRequest();
            objPersonalReportRequest.Firstname = firstName;
            objPersonalReportRequest.Lastname = lastName;
            objPersonalReportRequest.City = city;
            objPersonalReportRequest.Street = address;
            objPersonalReportRequest.State = state;
            objPersonalReportRequest.Zip = zipCode;
            objPersonalReportRequest.Ssn = ssn;

            var objPersonalReportResponse = await PersonalReportService.GetPersonalReportResponse(EntityType, entityId, objPersonalReportRequest);
            return objPersonalReportResponse;
        }

        #endregion Credit Check Verification

        #region Product Rules
        private IProductRuleResult VerifyRule(string entityType, string applicationNumber, RuleType ruleType, GroupName group,
            RuleName rule, Dictionary<string, string> referenceNumbers, string dataAttributeName = null)
        {
            ProductRuleResult ruleExecutionResult = null;
            var count = 1;
            const int maxAttempt = 3;

            //FaultRetry.RunWithAlwaysRetry(() =>
            // {
            var result = ProductRuleService.RunRule(entityType, applicationNumber, "Product1", ruleType.ToString(), group.ToString(), rule.ToString(), referenceNumbers, dataAttributeName).Result;

            if (result == null || !result.Any())
                throw new System.Exception("Unable to execute rule : " + rule);

            ruleExecutionResult = result.FirstOrDefault();

            //if (ruleExecutionResult == null || ruleExecutionResult.Result == false)
            //{
            //    if (count < maxAttempt)
            //    {
            //        count++;
            //        if (ruleExecutionResult != null && ruleExecutionResult.Result == false)
            //            throw new System.Exception("Unable to execute rule");

            //        throw new System.Exception("Unable to execute rule");
            //    }
            //}
            //// });

            //if (ruleExecutionResult == null)
            //    throw new System.Exception("Unable to execute rule");

            return ruleExecutionResult;
        }
        #endregion

        private ILoanAgreement GetLoanAgreementObject(IApplication application, IApplicant applicant, IDealOffer objOffer)
        {
            ILoanAgreement body = new LoanAgreement();
            if (objOffer != null)
            {
                body.PurchasePrice = "$" + objOffer.AmountFunded.ToString(); //multiply with refactor* 
                body.RecieptsPurchasedAmount = "$" + objOffer.RepaymentAmount.ToString();
                body.SpecificDailyAmmount = "$" + (objOffer.AmountFunded / 365).ToString(); //22 week working day * selected term // TODO
                body.AccountRoutingNumber = objOffer.RoutingNumber;
                body.AccountCheckingAccountNumber = objOffer.AccountNumber;
                body.BankName = objOffer.BankName;
                // body.SpecifiedPercentage = "10%";
            }
            body.SignedBy = application.ApplicantId;
            body.DBA = applicant.DBA;
            body.IPAddress = "10.1.1.99";
            body.ContactPerson = application.ContactFirstName + " " + application.ContactLastName;
            body.ContactPhone = application.PrimaryPhone.Phone;
            body.Branch = ""; //branch is not available in bankdetails.
            body.BusinessType = LookupService.GetLookupEntry("businessTypes", applicant.BusinessType).SingleOrDefault().Value;
            body.OpportunityDocPaymentType = " ";
            body.OpportunityOrginizationFee = " ";  //jes will send the calculation
            body.City = application.PrimaryAddress.City;
            body.State = application.PrimaryAddress.State;
            body.Address = application.PrimaryAddress.AddressLine1;
            body.Zip = application.PrimaryAddress.ZipCode;
            body.BusinessStartDate = applicant.BusinessStartDate.ToString("MM/dd/yyyy");
            body.LegalBusinessName = applicant.LegalBusinessName;
            body.FederalTaxID = applicant.BusinessTaxID;
            //images
            body.Logo = ApplicationProcessorConfiguration.ImageConfiguration.Logo;
            if (applicant.Owners.Count > 0)
            { body.Owners = applicant.Owners; }
            return body;
        }

        private bool EligibilityCheck(string entityType, string entityId)
        {

            var eligibilityResult = VerifyRule(entityType, entityId, RuleType.EligibilityCritiria, GroupName.EligibilityCheck, RuleName.CheckEligibility, null, ApplicationProcessorConfiguration.DataAttributes.EligibilityCheck);


            return eligibilityResult.Result;
        }

        private static bool? GetResultValue(dynamic data)
        {
            Func<dynamic> resultProperty = () => data;
            return HasProperty(resultProperty) ? GetValue(resultProperty) : null;
        }

        private static bool HasProperty<T>(Func<T> property)
        {
            try
            {
                property();
                return true;
            }
            catch (RuntimeBinderException)
            {
                return false;
            }
        }

        private static T GetValue<T>(Func<T> property)
        {
            return property();
        }            
      

        private void EnsureBusinessInputIsValid(ISubmitBusinessApplicationRequest applicationRequest,string Email)
        {
            if (applicationRequest == null)
                throw new ArgumentNullException(nameof(applicationRequest), "application cannot be empty");

            if (string.IsNullOrEmpty(applicationRequest.Phone))
                throw new ArgumentException("Personal mobile/Phone Number is mandatory");

            if (!Regex.IsMatch(Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
                throw new ArgumentException("Email Address is not valid");

            if (string.IsNullOrEmpty(Email))
                throw new ArgumentException("Personal email address is mandatory");

            if (applicationRequest.RequestedAmount <= 0)
                throw new ArgumentNullException("Requested amount is mandatory");

            if (string.IsNullOrWhiteSpace(applicationRequest.LegalBusinessName))
                throw new ArgumentNullException($"{nameof(applicationRequest.LegalBusinessName)} is mandatory");

            if (applicationRequest.LegalBusinessName.Length >= 255)
                throw new ArgumentNullException($"{nameof(applicationRequest.LegalBusinessName)} max length is 255");
            if (applicationRequest.AddressLine1.Length >= 50)
                throw new ArgumentNullException($"{nameof(applicationRequest.AddressLine1)} max length is 50");
            if (applicationRequest.City.Length >= 50)
                throw new ArgumentNullException($"{nameof(applicationRequest.City)} max length is 50");
            if (applicationRequest.ZipCode.Length >= 50)
                throw new ArgumentNullException($"{nameof(applicationRequest.ZipCode)} max length is 50");          
            if (applicationRequest.Owners == null)
                throw new ArgumentNullException($"{nameof(applicationRequest.Owners)} Information is needed");
            if (applicationRequest.Owners[0].FirstName == null)
                throw new ArgumentNullException("Owners First Name is needed");
            if (applicationRequest.Owners[0].LastName == null)
                throw new ArgumentNullException("Owners Last Name is needed");
            if (applicationRequest.Owners[0].SSN == null)
                throw new ArgumentNullException("Owners SSN is needed");          

            if (string.IsNullOrWhiteSpace(applicationRequest.AddressLine1))
                throw new ArgumentException("Current address line1 is mandatory");

            if (string.IsNullOrWhiteSpace(applicationRequest.City))
                throw new ArgumentNullException("Current address city is mandatory");

            if (string.IsNullOrWhiteSpace(applicationRequest.State))
                throw new ArgumentNullException("Current address state is mandatory");

            if (string.IsNullOrWhiteSpace(applicationRequest.ZipCode))
                throw new ArgumentNullException("Current address ZipCode is mandatory");
        }

        private IApplicantRequest GetBusinessApplicantRequest(ISubmitBusinessApplicationRequest request,string Email)
        {
            IApplicantRequest applicantRequest = new ApplicantRequest();
            

            #region Basic Information

            if (!string.IsNullOrEmpty(request.ApplicantId))
            {
                applicantRequest.Id = request.ApplicantId;
            }
            else
            {
                applicantRequest.UserName = Email;
                applicantRequest.Password = "sigma123";
            }
            applicantRequest.LegalBusinessName = request.LegalBusinessName;
            applicantRequest.DBA = request.DBA;
            applicantRequest.EIN = request.EIN;         

            #endregion Basic Information

            #region Primary ContactDetails

            applicantRequest.PrimaryAddress = new LendFoundry.Business.Applicant.Address
            {
                AddressLine1 = request.AddressLine1,
                AddressType = LendFoundry.Business.Applicant.AddressType.Business,
                City = request.City,
                ZipCode = request.ZipCode,
                State = request.State,
                IsDefault = true,
                Country = "USA"
            };
            applicantRequest.PrimaryPhone = new LendFoundry.Business.Applicant.PhoneNumber
            {
                IsDefault = true,
                Phone = request.Phone,
                PhoneType = LendFoundry.Business.Applicant.PhoneType.Mobile
            };
            applicantRequest.PrimaryEmail = new LendFoundry.Business.Applicant.EmailAddress
            {
                Email = Email,
                EmailType = LendFoundry.Business.Applicant.EmailType.Work,
                IsDefault = true
            };
            applicantRequest.PrimaryFax = null;

            #endregion Primary ContactDetails

            #region Owners Information

            applicantRequest.Owners = request.Owners;

            #endregion Owners Information

            #region ContactDetails

            applicantRequest.PhoneNumbers = new List<LendFoundry.Business.Applicant.IPhoneNumber>
            {
                new LendFoundry.Business.Applicant.PhoneNumber
                {
                    IsDefault = true,
                    Phone = request.Phone,
                    PhoneType = LendFoundry.Business.Applicant.PhoneType.Mobile
                }
            };

            applicantRequest.EmailAddresses = new List<LendFoundry.Business.Applicant.IEmailAddress>
            {
                new LendFoundry.Business.Applicant.EmailAddress
                {
                    Email = Email,
                    EmailType = LendFoundry.Business.Applicant.EmailType.Work,
                    IsDefault = true
                }
            };
            applicantRequest.Addresses = new List<LendFoundry.Business.Applicant.IAddress>
                    {
                        new LendFoundry.Business.Applicant.Address {
                            AddressLine1 = request.AddressLine1,
                            AddressType = LendFoundry.Business.Applicant.AddressType.Business,
                            City = request.City,
                            ZipCode = request.ZipCode,
                            State = request.State,
                            IsDefault = true
                        }
            };

            #endregion ContactDetails

            return applicantRequest;
        }

        private IApplicationRequest GetBusinessApplicationRequest(ISubmitBusinessApplicationRequest request,string Email)
        {
            

            IApplicationRequest applicationRequest = new ApplicationRequest
            {
                LeadOwnerId = request.LeadOwnerId,
                PrimaryApplicant = GetBusinessApplicantRequest(request,Email),              
                RequestedAmount = request.RequestedAmount,
                ContactFirstName = request.Owners[0].FirstName,
                ContactLastName = request.Owners[0].LastName,

                #region Primary ContactDetails

                PrimaryEmail = new LendFoundry.Business.Application.EmailAddress
                {
                    Email = Email,
                    EmailType = LendFoundry.Business.Application.EmailType.Work,
                    IsDefault = true
                },
                PrimaryAddress = new LendFoundry.Business.Application.Address
                {
                    AddressLine1 = request.AddressLine1,
                    AddressType = LendFoundry.Business.Application.AddressType.Work,
                    City = request.City,
                    ZipCode = request.ZipCode,
                    State = request.State,
                    IsDefault = true
                },
                PrimaryPhone = new LendFoundry.Business.Application.PhoneNumber
                {
                    IsDefault = true,
                    Phone = request.Phone,
                    PhoneType = LendFoundry.Business.Application.PhoneType.Mobile
                },
                PrimaryFax = null,

                #endregion Primary ContactDetails
            };

            return applicationRequest;
        }

        private ISelfDeclareInformation GetSelfInfomrationDetails(double annualRevenue, double averageBankBalances, bool haveExistingLoan)
        {
            ISelfDeclareInformation selfDeclareInformation = new SelfDeclareInformation();
            selfDeclareInformation.AnnualRevenue = annualRevenue;
            selfDeclareInformation.AverageBankBalance = averageBankBalances;
            selfDeclareInformation.IsExistingBusinessLoan = haveExistingLoan;
            return selfDeclareInformation;
        }

        private ISource GetSource(string Source)
        {
            ISource source = new Source();
            var SourceValues = LookupService.GetLookupEntry("souceType", Source).SingleOrDefault();
            source.SourceReferenceId = SourceValues.Key;
            source.SourceType = SourceValues.Key;
            return source;
        }

        #endregion Private Methods

        #region PayNet

        public async Task<ISearchCompanyResponse> SearchCompany(string entityId, ISearchCompanyRequest request)
        {
            LendFoundry.Syndication.Paynet.Request.ISearchCompanyRequest objRequest = new LendFoundry.Syndication.Paynet.Request.SearchCompanyRequest();

            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            if (application == null)
                throw new InvalidOperationException("Application not found");

            objRequest.CompanyName = request.CompanyName;
            objRequest.TaxId = request.TaxId;
            objRequest.Alias = request.Alias;
            objRequest.Address = request.Address;
            objRequest.City = request.City;
            objRequest.StateCode = request.State;
            objRequest.Phone = request.Phone;

            var objReport = await PaynetReportService.SearchCompany(EntityType, entityId, objRequest);
            return objReport;
        }

        public async Task<bool> GetPaynetReport(string entityId, string paynetId)
        {
            bool result = false;
            LendFoundry.Syndication.Paynet.Request.GetReportRequest objReportRequest = new LendFoundry.Syndication.Paynet.Request.GetReportRequest();
            objReportRequest.PaynetId = paynetId;
            objReportRequest.ReportFormat = ApplicationProcessorConfiguration.PaynetReportFormat;

            var objReportResponse = await PaynetReportService.GetCompanyReport(EntityType, entityId, objReportRequest);
            if (objReportRequest.ReportFormat.Equals((PaynetReportFormat.XML).ToString()))
            {
                result = true;
            }
            else
            {
                byte[] fileByte;
                string fileName;
                if (objReportResponse.filesField.Count != 0)
                {
                    fileByte = objReportResponse.filesField.FirstOrDefault().bytesField;
                    fileName = objReportResponse.filesField.FirstOrDefault().fileNameField;
                }
                else
                {
                    fileByte = Encoding.ASCII.GetBytes(objReportResponse.htmlField);
                    fileName = entityId + ".html";
                }
                var objApplicationDocument = await ApplicationDocumentService.Add(entityId, "Paynet", fileByte, fileName, null, null);
                if (objApplicationDocument != null && !string.IsNullOrEmpty(objApplicationDocument.DocumentId))
                {
                    result = true;
                }
            }

            return result;
        }

        #endregion PayNet

        #region Perform CreditCheck Verification

        public async Task<string> PerformCreditCheckVerification(string entityId)
        {
            string result = string.Empty;

            var referencenumber = Guid.NewGuid().ToString("N");

            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            if (application == null)
                throw new InvalidOperationException("Application not found");

            //var applicant = await BusinessApplicantService.Get(application.ApplicantId);

            //if (applicant == null)
            //    throw new InvalidOperationException("Applicant not found");

            //var objPersonalReportResponse = CallExperianPersonalSyndication(entityId, applicant.Owners[0].FirstName, applicant.Owners[0].LastName, applicant.Owners[0].SSN,
            //   applicant.Owners[0].Addresses[0].AddressLine1, applicant.Owners[0].Addresses[0].City,
            //   applicant.Owners[0].Addresses[0].State, applicant.Owners[0].Addresses[0].ZipCode);

            await EventHub.Publish(new CreditCheckVerification
            {
                EntityId = application.ApplicationNumber,
                EntityType = EntityType,
                Response = null,
                Request = "PerformCreditCheckVerification",
                ReferenceNumber = referencenumber
            });
            result = "CreditCheck Verification Completed"; //TODO
            return result;
        }

        #endregion Perform CreditCheck Verification

        #region Perform Business Tax Id Verification

        public async Task<string> PerformBusinessTaxIdVerification(string entityId)
        {
            string result = string.Empty;

            var referencenumber = Guid.NewGuid().ToString("N");

            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            if (application == null)
                throw new InvalidOperationException("Application not found");

            // var objSyndicationResponses = await DataAttributeService.GetAttribute(EntityType, entityId, new List<string> { "datamerchReport" });

            await EventHub.Publish(new BusinessTaxIdVerification
            {
                EntityId = application.ApplicationNumber,
                EntityType = EntityType,
                Response = null,
                Request = "PerformBusinessTaxIdVerification",
                ReferenceNumber = referencenumber
            });
            result = "Business Tax Id Verification Completed"; //TODO
            return result;
        }

        #endregion Perform Business Tax Id Verification

        #region Perform Business Review CreditRisk

        public async Task<string> PerformBusinessReviewCreditRisk(string entityId)
        {
            string result = string.Empty;

            var referencenumber = Guid.NewGuid().ToString("N");

            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            if (application == null)
                throw new InvalidOperationException("Application not found");

            await EventHub.Publish(new BusinessCreditRiskVerification
            {
                EntityId = application.ApplicationNumber,
                EntityType = EntityType,
                Response = null,
                Request = "PerformBusinessReviewCreditRisk",
                ReferenceNumber = referencenumber
            });
            result = "Business Review CreditRisk Completed"; //TODO
            return result;
        }

        #endregion Perform Business Review CreditRisk

        #region Get Application Details

        public async Task<IApplicationDetails> GetApplicationDetails(string entityId)
        {
            var objResponse = await BusinessApplicationService.GetApplicationDetails(entityId);
            return objResponse;
        }

        #endregion Get Application Details

        #region Edit Application

        public async Task<IApplicationDetails> EditApplication(string entityId, ISubmitBusinessApplicationRequest request)
        {
             string Email = ExtractEmail(request);
            EnsureBusinessInputIsValid(request, Email);

            var applicationRequest = GetBusinessApplicationRequest(request, Email);

            await BusinessApplicationService.UpdateApplication(entityId, applicationRequest);
            var objResponse = await BusinessApplicationService.GetApplicationDetails(entityId);
            return objResponse;
        }

        #endregion Edit Application

        #region Add Deal

        public async Task<IApplicationOffer> AddDeal(string entityId, IDealOfferRequest request)
        {
            var offer = await Task.Run(() => OfferEngineService.AddDeal(EntityType, entityId, request));
            EntityStatusService.ChangeStatus(EntityType, entityId, ApplicationProcessorConfiguration.Statuses["OfferGiven"]);
            return offer;
        }

        #endregion Add Deal

        #region  DocuSign Event Listner
        public async Task<string> DocuSignEventListner(string entityId, IDocusignEventRequest docusignEventRequest)
        {
            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            if (application == null)
                throw new InvalidOperationException("Application not found");

            if (docusignEventRequest.Status == LendFoundry.Syndication.LFDocuSign.TemplateConfiguration.DocusignEventsType.Completed.ToString())
            {
                var referencenumber = Guid.NewGuid().ToString("N");

                await EventHub.Publish(new LoanAgreementSigned
                {
                    EntityId = entityId,
                    EntityType = EntityType,
                    Response = null,
                    Request = docusignEventRequest,
                    ReferenceNumber = referencenumber
                });
                EntityStatusService.ChangeStatus(EntityType, entityId, ApplicationProcessorConfiguration.Statuses["OfferAccepted"]);
                EntityStatusService.ChangeStatus(EntityType, entityId, ApplicationProcessorConfiguration.Statuses["FundingReview"]);
            }
            return "Success";
        }
        #endregion

        #region LexisNexis

        #region Bankruptcy Search

        public async Task<IBankruptcySearchResponse> BankruptcySearch(string entityId, Request.ISearchBankruptcyRequest searchRequest)
        {
            LendFoundry.Syndication.LexisNexis.Bankruptcy.SearchBankruptcyRequest objRequest = new LendFoundry.Syndication.LexisNexis.Bankruptcy.SearchBankruptcyRequest();

            objRequest.FirstName = searchRequest.FirstName;
            objRequest.LastName = searchRequest.LastName;
            objRequest.Ssn = searchRequest.Ssn;

            var objSearchResponse = await LexisNexisService.BankruptcySearch(EntityType, entityId, objRequest);
            return objSearchResponse;
        }

        #endregion Bankruptcy Search

        #region Bankruptcy Report

        public async Task<IBankruptcyReportResponse> BankruptcyReport(string entityId, Request.IBankruptcyReportRequest reportRequest)
        {
            LendFoundry.Syndication.LexisNexis.Bankruptcy.BankruptcyReportRequest objRequest = new LendFoundry.Syndication.LexisNexis.Bankruptcy.BankruptcyReportRequest();

            objRequest.UniqueId = reportRequest.UniqueId;
            objRequest.QueryId = reportRequest.QueryId;
            var objReportResponse = await LexisNexisService.BankruptcyReport(EntityType, entityId, objRequest);

            return objReportResponse;
        }

        #endregion Bankruptcy Report

        #region Criminal Search

        public async Task<ICriminalRecordSearchResponse> CriminalRecordSearch(string entityId, ICriminalRecordSearchRequest searchRequest)
        {
            LendFoundry.Syndication.LexisNexis.CriminalRecord.CriminalRecordSearchRequest objRequest = new LendFoundry.Syndication.LexisNexis.CriminalRecord.CriminalRecordSearchRequest();
            objRequest.FirstName = searchRequest.FirstName;
            objRequest.LastName = searchRequest.LastName;
            objRequest.Ssn = searchRequest.Ssn;

            var objSearchResponse = await LexisNexisService.CriminalSearch(EntityType, entityId, objRequest);
            return objSearchResponse;
        }

        #endregion Criminal Search

        #region CriminalRecord Report

        public async Task<ICriminalReportResponse> CriminalRecordReport(string entityId, ICriminalRecordReportRequest reportRequest)
        {
            LendFoundry.Syndication.LexisNexis.CriminalRecord.CriminalRecordReportRequest objRequest = new LendFoundry.Syndication.LexisNexis.CriminalRecord.CriminalRecordReportRequest();
            objRequest.UniqueId = reportRequest.UniqueId;
            objRequest.QueryId = reportRequest.QueryId;
            var objReportResponse = await LexisNexisService.CriminalRecordReport(EntityType, entityId, objRequest);

            return objReportResponse;
        }
        #endregion CriminalRecord Report

        #endregion LexisNexis

        #region fund application
        public async Task<string> AddFundingRequest(string entityid)
        {
            var application = await BusinessApplicationService.GetByApplicationNumber(entityid);

            if (application == null)
                throw new InvalidOperationException("Application not found");

            EntityStatusService.ChangeStatus(EntityType, entityid, ApplicationProcessorConfiguration.Statuses["Approved"]);

            return "Funding request added successfuly";
        }
        #endregion
    }
}
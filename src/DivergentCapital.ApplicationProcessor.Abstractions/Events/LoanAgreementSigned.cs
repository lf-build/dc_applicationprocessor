﻿using CreditExchange.SyndicationStore.Events;

namespace DivergentCapital.ApplicationProcessor.Events
{
    public class LoanAgreementSigned: SyndicationCalledEvent
    {
    }
}

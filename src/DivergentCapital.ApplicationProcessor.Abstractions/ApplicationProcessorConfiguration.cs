﻿using LendFoundry.Security.Identity;

namespace DivergentCapital.ApplicationProcessor
{
    public class ApplicationProcessorConfiguration
    {
        public CaseInsensitiveDictionary<string> Statuses { get; set; }
        public CaseInsensitiveDictionary<RuleDetail> Rules { get; set; }
        public string[] ReApplyStatuseCodes { get; set; }
        public int ReApplyTimeFrameDays { get; set; }
        public string PrimaryCreditBureau { get; set; }
        public string SecondaryCreditBureau { get; set; }
        public string PlaidUIEndpoint { get; set; }
        public string YodleeUIEndpoint { get; set; }
        public string PaynetReportFormat { get; set; }
        public ImageConfiguration ImageConfiguration { get; set; }

        public DataAttributes DataAttributes { get; set; }
    }
}
﻿
namespace DivergentCapital.ApplicationProcessor
{
    public enum RuleName
    {
        CheckEligibility = 1,
        CheckPScore = 2
    }
}
﻿namespace DivergentCapital.ApplicationProcessor
{
    public enum PaynetReportFormat
    {
        PDF,
        HTML,
        XML,
        BNDL,
        CDAT
    }
}
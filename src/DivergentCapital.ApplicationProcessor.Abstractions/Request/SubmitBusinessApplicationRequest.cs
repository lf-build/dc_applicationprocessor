﻿using LendFoundry.Business.Applicant;
using LendFoundry.Business.Application;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DivergentCapital.ApplicationProcessor.Request
{
    public class SubmitBusinessApplicationRequest : ISubmitBusinessApplicationRequest
    {
        public string LegalBusinessName { get; set; }
        public string DBA { get; set; }
        public string EIN { get; set; }
        public string AddressLine1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public double RequestedAmount { get; set; }
        public string ApplicationNumber { get; set; }
        public string ApplicantId { get; set; }
        public string LeadOwnerId { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IOwner, Owner>))]
        public IList<IOwner> Owners { get; set; }
    }
}
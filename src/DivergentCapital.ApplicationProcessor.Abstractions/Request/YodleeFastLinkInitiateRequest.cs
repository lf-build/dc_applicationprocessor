﻿namespace DivergentCapital.ApplicationProcessor.Request
{
    public class YodleeFastLinkInitiateRequest : IYodleeFastLinkInitiateRequest
    {
        public string CallBackUrl { get; set; }
    }
}

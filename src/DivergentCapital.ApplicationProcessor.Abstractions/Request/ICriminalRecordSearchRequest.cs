﻿namespace DivergentCapital.ApplicationProcessor.Request
{
    public interface ICriminalRecordSearchRequest
    {
        string Ssn { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
    }
}
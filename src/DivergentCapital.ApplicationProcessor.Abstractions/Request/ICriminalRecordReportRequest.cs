﻿namespace DivergentCapital.ApplicationProcessor.Request
{
    public interface ICriminalRecordReportRequest
    {
        string UniqueId { get; set; }
        string QueryId { get; set; }
    }
}
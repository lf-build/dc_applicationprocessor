﻿namespace DivergentCapital.ApplicationProcessor.Request
{
    public class SearchBankruptcyRequest : ISearchBankruptcyRequest
    {
        public string Ssn { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
﻿namespace DivergentCapital.ApplicationProcessor.Request
{
    public class BankruptcyReportRequest : IBankruptcyReportRequest
    {
        public string UniqueId { get; set; }
        public string QueryId { get; set; }
    }
}
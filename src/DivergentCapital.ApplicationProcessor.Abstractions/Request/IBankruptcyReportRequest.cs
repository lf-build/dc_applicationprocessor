﻿namespace DivergentCapital.ApplicationProcessor.Request
{
    public interface IBankruptcyReportRequest
    {
        string UniqueId { get; set; }
        string QueryId { get; set; }
    }
}
﻿namespace DivergentCapital.ApplicationProcessor.Request
{
    public interface ISearchBankruptcyRequest
    {
        string Ssn { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
    }
}
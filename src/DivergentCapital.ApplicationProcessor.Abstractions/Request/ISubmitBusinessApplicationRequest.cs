﻿using LendFoundry.Business.Applicant;
using System.Collections.Generic;

namespace DivergentCapital.ApplicationProcessor.Request
{
    public interface ISubmitBusinessApplicationRequest
    {
        string LegalBusinessName { get; set; }
        string DBA { get; set; }
        string EIN { get; set; }
        string AddressLine1 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string ZipCode { get; set; }
        string Phone { get; set; }
        double RequestedAmount { get; set; }
        IList<IOwner> Owners { get; set; }     
        string ApplicationNumber { get; set; }
        string ApplicantId { get; set; }
        string LeadOwnerId { get; set; }
    }
}
﻿namespace DivergentCapital.ApplicationProcessor.Request
{
    public interface IYodleeFastLinkInitiateRequest
    {
        string CallBackUrl { get; set; }
    }
}

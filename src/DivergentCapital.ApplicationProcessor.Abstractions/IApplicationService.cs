﻿using DivergentCapital.ApplicationProcessor.Request;
using CreditExchange.Syndication.Plaid.Response;
using LendFoundry.Business.Application;
using LendFoundry.Syndication.Experian.Response;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Business.OfferEngine;
using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport;

namespace DivergentCapital.ApplicationProcessor
{
    public interface IApplicationProcessorClientService
    {
        Task<LendFoundry.Business.Application.IApplicationResponse> SubmitBusinessApplication(ISubmitBusinessApplicationRequest submitBusinessApplicationRequest);

        Task<ITransactionsAndAccountsResponse> PlaidCalculateCashFlow(string entityId, IPlaidBankAccountRequest plaidBankAccountRequest);

        Task<ITransactionsAndAccountsResponse> PlaidReCalculateCashFlow(string entityId);

        Task<IApplicationResponse> AddLead(ISubmitBusinessApplicationRequest submitApplicationRequest);      

        Task<bool> SubmitDocument(string entityId, string category, byte[] fileBytes, string fileName, List<string> tags);

        Task<bool> SignConsent(string entityId, string category, object body);

        Task<IGetSearchBusinessResponse> BusinessSearch(string entityId, IBusinessSearchRequest request);

        Task<IGetBusinessReportResponse> GetBusinessReport(string entityId, IBusinessReportRequest request);

        Task<LendFoundry.Syndication.Experian.Response.IGetPersonalReportResponse> GetPersonalReport(string entityId, IPersonalReportRequest request);

        Task<string> ChooseOffer(string entityId, string offerId);

        Task<LendFoundry.Syndication.Paynet.Response.ISearchCompanyResponse> SearchCompany(string entityId, ISearchCompanyRequest request);

        Task<bool> GetPaynetReport(string entityId, string paynetId);

        Task<string> PerformBusinessVerification1(string entityId);

        Task<string> PerformBusinessVerification2(string entityId);

        Task<string> ResendBankLinking(string entityId);

        Task<IApplicationOffer> ComputeOffer(string entityId);

        Task<string> CalculateManualCashflow(string entityId);

        Task<string> EditOffer(string entityId);

        Task<IList<IBusinessLoanOffer>> PresentOffer(string entityId);

        Task<string> GenerateAndSendAgreement(string entityId);

        Task<string> StipulateDocs(string entityId);

        Task<string> PerformCreditCheckVerification(string entityId);

        Task<string> PerformBusinessTaxIdVerification(string entityId);

        Task<string> PerformBusinessReviewCreditRisk(string entityId);

        Task<IApplicationDetails> GetApplicationDetails(string applicationId);

        Task<IApplicationDetails> EditApplication(string entityId, ISubmitBusinessApplicationRequest request);

        Task<IApplicationOffer> AddDeal(string entityId, IDealOfferRequest request);
        Task<string> DocuSignEventListner(string entityId, IDocusignEventRequest docusignEventRequest);

        Task<IBankruptcySearchResponse> BankruptcySearch(string entityid, Request.ISearchBankruptcyRequest request);

        Task<IBankruptcyReportResponse> BankruptcyReport(string entityid, Request.IBankruptcyReportRequest request);

        Task<ICriminalRecordSearchResponse> CriminalRecordSearch(string entityid, ICriminalRecordSearchRequest request);

        Task<ICriminalReportResponse> CriminalRecordReport(string entityid, ICriminalRecordReportRequest request);

        Task<string> AddFundingRequest(string entityid);
    }
}
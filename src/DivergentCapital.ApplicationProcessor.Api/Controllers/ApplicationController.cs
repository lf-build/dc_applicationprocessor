﻿using DivergentCapital.ApplicationProcessor.Request;
using LendFoundry.Business.OfferEngine;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Net.Http.Headers;
using RestSharp.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DivergentCapital.ApplicationProcessor.Api.Controllers
{
    [Route("/application")]
    public class ApplicationController : ExtendedController
    {
        #region Constructor

        public ApplicationController(IApplicationProcessorClientService applicationService)
        {
            if (applicationService == null)
                throw new ArgumentNullException(nameof(applicationService));

            ApplicationService = applicationService;
        }

        #endregion Constructor

        #region Private Variables

        private IApplicationProcessorClientService ApplicationService { get; }

        #endregion Private Variables

        #region Submit Application

        [HttpPost("submit/business")]
        public async Task<IActionResult> SubmitApplication([FromBody]SubmitBusinessApplicationRequest submitBusinessApplicationRequest)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationService.SubmitBusinessApplication(submitBusinessApplicationRequest)));
        }

        #endregion Submit Application

        #region Add Lead

        [HttpPost("lead")]
        public async Task<IActionResult> AddLead([FromBody]SubmitBusinessApplicationRequest submitApplicationRequest)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationService.AddLead(submitApplicationRequest)));
        }

        #endregion Add Lead

        #region Calculate Plaid Cashflow

        [HttpPost("plaid/cashflow/{entityid}")]
        public async Task<IActionResult> CalculatePlaidCashflow(string entityId, [FromBody]PlaidBankAccountRequest plaidBankAccountRequest)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationService.PlaidCalculateCashFlow(entityId, plaidBankAccountRequest)));
        }

        #endregion Calculate Plaid Cashflow

        #region Re-Calculate Plaid Cashflow

        [HttpPost("plaid/recalculatecashflow/{entityid}")]
        public async Task<IActionResult> ReCalculatePlaidCashflow(string entityId)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationService.PlaidReCalculateCashFlow(entityId)));
        }

        #endregion Re-Calculate Plaid Cashflow   

        #region Submit Document

        [HttpPost("{entityid}/{category}/{*tags}")]
        public async Task<IActionResult> SubmitDocument(string entityid, string category, IFormFile file, string tags)
        {
            if (file == null)
                return ErrorResult.BadRequest("Please upload file.");

            var fileName = ContentDispositionHeaderValue
                .Parse(file.ContentDisposition)
                .FileName
                .Trim('"');

            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.SubmitDocument(entityid, category,
                            file.OpenReadStream().ReadAsBytes(), fileName, TagValidation(tags))));
        }

        #endregion Submit Document

        #region Sign Consent

        [HttpPost("{entityid}/{category}/consent/sign")]
        public async Task<IActionResult> Sign(string entityid, string category, [FromBody]object body)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.SignConsent(entityid, category, body)));
        }

        #endregion Sign Consent

        #region Get Business Report

        [HttpPost("{entityid}/experian/business/search")]
        public async Task<IActionResult> BusinessSearch(string entityid, [FromBody]BusinessSearchRequest request)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.BusinessSearch(entityid, request)));
        }

        [HttpPost("{entityid}/experian/business/report")]
        public async Task<IActionResult> GetBusinessReport(string entityid, [FromBody]BusinessReportRequest request)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.GetBusinessReport(entityid, request)));
        }

        #endregion Get Business Report

        #region Get Personal Report

        [HttpPost("{entityid}/report/personal")]
        public async Task<IActionResult> GetPersonalReport(string entityid, [FromBody]PersonalReportRequest request)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.GetPersonalReport(entityid, request)));
        }

        #endregion Get Personal Report

        #region Choose Final Offer

        [HttpGet("{entityid}/{offerId}/offers/final")]
        public async Task<IActionResult> ChooseOffer(string entityid, string offerId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.ChooseOffer(entityid, offerId)));
        }

        #endregion Choose Final Offer

        #region Perform Business Verification 1

        [HttpGet("{entityid}/businessverification1")]
        public async Task<IActionResult> PerformBusinessVerification1(string entityId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.PerformBusinessVerification1(entityId)));
        }

        #endregion Perform Business Verification 1

        #region Perform Business Verification 2

        [HttpGet("{entityid}/businessverification2")]
        public async Task<IActionResult> PerformBusinessVerification2(string entityId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.PerformBusinessVerification2(entityId)));
        }

        #endregion Perform Business Verification 2

        #region Resend Bank Linking

        [HttpGet("{entityid}/resend/banklink")]
        public async Task<IActionResult> ResendBankLinking(string entityId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.ResendBankLinking(entityId)));
        }

        #endregion Resend Bank Linking

        #region Compute Offers

        [HttpGet("{entityid}/compute/offer")]
        public async Task<IActionResult> ComputeOffer(string entityId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.ComputeOffer(entityId)));
        }

        #endregion Compute Offers

        #region Calculate Manual Cashflow

        [HttpGet("plaid/cashflow/manual/{entityid}")]
        public async Task<IActionResult> CalculateManualCashflow(string entityId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.CalculateManualCashflow(entityId)));
        }

        #endregion Calculate Manual Cashflow

        #region Edit Offer

        [HttpGet("{entityid}/edit/offer")]
        public async Task<IActionResult> EditOffer(string entityId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.EditOffer(entityId)));
        }

        #endregion Edit Offer

        #region Present Offer

        [HttpGet("{entityid}/present/offer")]
        public async Task<IActionResult> PresentOffer(string entityId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.PresentOffer(entityId)));
        }

        #endregion Present Offer

        #region Generate And Send Agreement

        [HttpGet("{entityid}/generate/agreement")]
        public async Task<IActionResult> GenerateAndSendAgreement(string entityId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.GenerateAndSendAgreement(entityId)));
        }

        #endregion Generate And Send Agreement

        #region Stipulate Docs

        [HttpGet("{entityid}/stipulate/docs")]
        public async Task<IActionResult> StipulateDocs(string entityId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.StipulateDocs(entityId)));
        }

        #endregion Stipulate Docs

        #region Private Methods

        private static List<string> TagValidation(string tags)
        {
            if (string.IsNullOrWhiteSpace(tags))
                return null;

            tags = WebUtility.UrlDecode(tags);
            var tagList = SplitTags(tags);
            return tagList.ToList();
        }

        private static IEnumerable<string> SplitTags(string tags)
        {
            return string.IsNullOrWhiteSpace(tags)
                ? new List<string>()
                : tags.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        #endregion Private Methods

        #region Paynet

        #region Search Company

        [HttpPost("{entityid}/paynet/search")]
        public async Task<IActionResult> SearchPaynetCompany(string entityid, [FromBody]SearchCompanyRequest request)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.SearchCompany(entityid, request)));
        }

        #endregion Search Company

        #region Get Company Report

        [HttpGet("{entityid}/{paynetId}/paynet/report")]
        public async Task<IActionResult> GetPaynetReport(string entityid, string paynetId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.GetPaynetReport(entityid, paynetId)));
        }

        #endregion Get Company Report

        #endregion Paynet

        #region Perform CreditCheck Verification

        [HttpGet("{entityid}/creditcheckverification")]
        public async Task<IActionResult> PerformCreditCheckVerification(string entityId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.PerformCreditCheckVerification(entityId)));
        }

        #endregion Perform CreditCheck Verification

        #region Perform Business Tax Id Verification

        [HttpGet("{entityid}/businesstaxidverification")]
        public async Task<IActionResult> PerformBusinessTaxIdVerification(string entityId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.PerformBusinessTaxIdVerification(entityId)));
        }

        #endregion Perform Business Tax Id Verification

        #region Perform Business Review CreditRisk

        [HttpGet("{entityid}/businessreviewcreditrisk")]
        public async Task<IActionResult> PerformBusinessReviewCreditRisk(string entityId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.PerformBusinessReviewCreditRisk(entityId)));
        }

        #endregion Perform Business Review CreditRisk

        #region Get Application Details

        [HttpGet("{entityid}/details")]
        public async Task<IActionResult> ApplicationDetails(string entityid)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.GetApplicationDetails(entityid)));
        }

        #endregion Get Application Details

        #region Edit Application

        [HttpPut("{entityid}/edit")]
        public async Task<IActionResult> EditApplication(string entityid, [FromBody]SubmitBusinessApplicationRequest submitBusinessApplicationRequest)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationService.EditApplication(entityid, submitBusinessApplicationRequest)));
        }

        #endregion Edit Application

        #region Add Deal

        [HttpPost("{entityid}/add/deal")]
        public async Task<IActionResult> AddDeal(string entityId, [FromBody]DealOfferRequest request)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationService.AddDeal(entityId, request)));
        }

        #endregion Add Deal

        #region  DocuSign Event Listner
        [HttpPost("{entityId}/docusign/listner")]
        public async Task<IActionResult> DocuSignEventListner(string entityId,[FromBody]DocusignEventRequest docusignEventRequest)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.DocuSignEventListner(entityId, docusignEventRequest)));
        }
        #endregion

        #region LexisNexis

        #region Bankruptcy search

        [HttpPost("{entityid}/bankruptcy/search")]
        public async Task<IActionResult> BankruptcySearch(string entityid, [FromBody] SearchBankruptcyRequest request)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.BankruptcySearch(entityid, request)));
        }

        #endregion Bankruptcy search

        #region Bankruptcy Report

        [HttpPost("{entityid}/bankruptcy/report")]
        public async Task<IActionResult> BankruptcyReport(string entityid, [FromBody]BankruptcyReportRequest request)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.BankruptcyReport(entityid, request)));
        }

        #endregion Bankruptcy Report

        #region Criminal Search

        [HttpPost("{entityid}/criminalrecord/search")]
        public async Task<IActionResult> CriminalRecordSearch(string entityid, [FromBody] CriminalRecordSearchRequest request)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.CriminalRecordSearch(entityid, request)));
        }

        #endregion Criminal Search

        #region Criminal Report

        [HttpPost("{entityid}/criminalrecord/report")]
        public async Task<IActionResult> CriminalRecordReport(string entityid, [FromBody] CriminalRecordReportRequest request)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.CriminalRecordReport(entityid, request)));
        }

        #endregion Criminal Report

        #endregion LexisNexis

        #region fund application
        [HttpPost("{entityid}/add/fundingrequest")]
        public async Task<IActionResult> AddFundingRequest(string entityid)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.AddFundingRequest(entityid)));
        }
        #endregion
    }
}
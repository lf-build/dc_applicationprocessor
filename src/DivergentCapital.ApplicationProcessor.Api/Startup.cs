﻿using CreditExchange.Application.Document.Client;
using CreditExchange.BBBSearch.Client;
using LendFoundry.DataAttributes.Client;
using CreditExchange.Datamerch.Client;
using CreditExchange.Email.Client;
using CreditExchange.Plaid.Client;
using CreditExchange.StatusManagement.Client;
using CreditExchange.Whitepages.Client;
using CreditExchange.Yelp.Client;
using CreditExchange.YodleeFastLink.Client;
using LendFoundry.Business.Application.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using CreditExchange.Consent.Client;
using LendFoundry.Experian.Client;
using CreditExchange.VerificationEngine.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.DocuSign.Client;
using LendFoundry.Paynet.Client;
using LendFoundry.Business.Applicant.Client;
using LendFoundry.Business.OfferEngine.Client;
using LendFoundry.Syndication.LexisNexis.Client;
using LendFoundry.ProductRule.Client;

namespace DivergentCapital.ApplicationProcessor.Api
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddConfigurationService<ApplicationProcessorConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);

            services.AddCors();
            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient(provider => provider.GetRequiredService<IConfigurationService<ApplicationProcessorConfiguration>>().Get());

            services.AddLookupService(Settings.LookUpService.Host, Settings.LookUpService.Port);
            services.AddStatusManagementService(Settings.StatusManagement.Host, Settings.StatusManagement.Port);
            services.AddApplicationService(Settings.BusinessApplication.Host, Settings.BusinessApplication.Port);
            services.AddApplicantService(Settings.BusinessApplicant.Host, Settings.BusinessApplicant.Port);
            services.AddDataAttributes(Settings.DataAttribute.Host, Settings.DataAttribute.Port);
            services.AddApplicantDocumentService(Settings.ApplicationDocument.Host, Settings.ApplicationDocument.Port);
            services.AddConsentService(Settings.Consent.Host, Settings.Consent.Port);
            services.AddOfferEngineService(Settings.BusinessOfferEngine.Host, Settings.BusinessOfferEngine.Port);
            services.AddProductRuleService(Settings.ProductRule.Host, Settings.ProductRule.Port);

            services.AddDatamerchService(Settings.DataMerchSyndication.Host, Settings.DataMerchSyndication.Port);
            services.AddWhitepagesService(Settings.WhitePagesSyndication.Host, Settings.WhitePagesSyndication.Port);
            services.AddYelpService(Settings.YelpSyndication.Host, Settings.YelpSyndication.Port);
            services.AddBBBSearchService(Settings.BBBSearchSyndication.Host, Settings.BBBSearchSyndication.Port);
            services.AddPlaidService(Settings.PlaidSyndication.Host, Settings.PlaidSyndication.Port);
            services.AddEmailService(Settings.Email.Host, Settings.Email.Port);
            services.AddYodleeFastLinkService(Settings.YodleeFastLinkSyndication.Host, Settings.YodleeFastLinkSyndication.Port);
            services.AddBusinessReportService(Settings.Experian.Host, Settings.Experian.Port);
            services.AddPersonalReportService(Settings.Experian.Host, Settings.Experian.Port);
            services.AddVerificationEngineService(Settings.VerificationEngine.Host, Settings.VerificationEngine.Port);
            services.AddDecisionEngine(Settings.DecisionEngine.Host, Settings.DecisionEngine.Port);
            services.AddDocuSignService(Settings.DocuSign.Host, Settings.DocuSign.Port);            
            services.AddPaynetService(Settings.PaynetSyndication.Host, Settings.PaynetSyndication.Port);
            services.AddLexisNexisService(Settings.LexisNexisSyndication.Host, Settings.LexisNexisSyndication.Port);

            services.AddTransient<IApplicationProcessorClientService, ApplicationService>();
            services.AddTransient<IStatusManagementService, StatusManagementService>();          
            services.AddMvc().AddLendFoundryJsonOptions();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMiddleware<RequestLoggingMiddleware>();
            app.UseEventHub();            
            app.UseMvc();
        }
    }
}